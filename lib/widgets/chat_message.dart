import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class ChatMessage extends StatelessWidget {
  final int user;
  final String message_content;
  final String username;
  final Color color;
  ChatMessage(
      {Key key, this.user, this.username, this.message_content, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Card(
        color: Colors.blue,
        child: InkWell(
          splashColor: Colors.blue.withAlpha(100),
          child: Container(
            //height: 55,
            padding: EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(username,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontFamily: 'Sora', fontSize: 12.00)),
                    Text(
                    message_content,
                    // overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: 'Sora',
                        fontSize: 18,
                        color: Colors.grey[800]),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
