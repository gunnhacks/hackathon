import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hackthon/pages/chat.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class TopicCard extends StatelessWidget {
  final String topicName;
  final String topicID;
  final int activeDebates;

  TopicCard({Key key, this.topicName, this.activeDebates, this.topicID})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        color: Colors.grey,
        child: InkWell(
            splashColor: Colors.blue.withAlpha(100),
            onTap: () async {
              var uid = await Firestore.instance
                  .collection("topics")
                  .where("name", isEqualTo: topicName)
                  .where("uid",
                      isEqualTo:
                          sha256.convert(utf8.encode(topicName)).toString())
                  .getDocuments();
              print(uid.documents.length);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          ChatPage(docid: uid.documents.first.data["chat"])));
            },
            child: Container(
                height: 55,
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 300,
                      child: Text(topicName,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontFamily: 'Sora',
                              fontSize: 20,
                              color: Colors.grey[800])),
                    ),
                    Text('Debates:\n' + activeDebates.toString(),
                        textAlign: TextAlign.center,
                        style: TextStyle(fontFamily: 'Sora')),
                  ],
                ))));
    /*child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          
          Text(topicName),
          Text('Active Debates: ' + activeDebates.toString())
        ],
      )*/
  }
}
