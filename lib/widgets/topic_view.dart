import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hackthon/widgets/topic_card.dart';
import './topic_card.dart';

class TopicView extends StatefulWidget {
  const TopicView({Key key}) : super(key: key);
  TopicViewState createState() => TopicViewState();
}

class TopicViewState extends State<TopicView> {
  @override
  Widget build(BuildContext context) {
    CollectionReference topics = Firestore.instance.collection('topics');
    print(topics.snapshots());
    return StreamBuilder(
        stream: topics.snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          return ListView(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              children: snapshot.data.documents.map((document) {
                return TopicCard(
                  topicName: document['name'],
                  activeDebates: document['debates'].length,
                  topicID: document['uid'],
                );
              }).toList());
        });
  }
}
