import 'package:flutter/material.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:hackthon/pages/login.dart';
void main() {
  //WidgetsFlutterBinding.ensureInitialized();
  runApp(App());
}

class App extends StatelessWidget {
    //final Future<FirebaseApp> _initialization = Firebase.initializeApp();

    @override
    Widget build(BuildContext context) {
      return new MaterialApp(
        title: 'Debate Thing',
        theme: new ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: new LoginPage(),
        
      );
    }
}
