import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hackthon/widgets/chat_message.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<String> getUsername(String uid) async {
  var db = Firestore.instance.collection('users');
  var docs = await db.where("uid", isEqualTo: uid).getDocuments();
  if (docs.documents.length == 0) {
    return uid;
  }
  return docs.documents.first.data["username"];
}

class ChatPage extends StatefulWidget {
  final docid;
  const ChatPage({Key key, this.docid}) : super(key: key);

  ChatPageState createState() => ChatPageState();
}

class ChatPageState extends State<ChatPage> {
  final controller = TextEditingController();
  final rescontroller = TextEditingController();
  final ScrollController scontroller = ScrollController();

  @override
  void dispose() {
    controller.dispose();
    rescontroller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    CollectionReference chats = Firestore.instance
        .collection('chats')
        .document(widget.docid)
        .collection("messages");
    print("docid is");
    print(widget.docid);
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.agriculture),
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) => AlertDialog(
                    title: const Text('Create a resolution'),
                    content: new Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        TextField(
                          controller: rescontroller,
                          keyboardType: TextInputType.multiline,
                          minLines: 2,
                          maxLines: null,
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () async {
                          String res = rescontroller.text;
                          FirebaseUser user =
                              await FirebaseAuth.instance.currentUser();
                          String uname = await getUsername(user.uid);
                          var resdb = Firestore.instance.collection('resolved');
                          await resdb.add({
                            "content": res,
                            "user": uname,
                            "chat": widget.docid,
                          });
                          Navigator.of(context).pop();
                        },
                        textColor: Theme.of(context).primaryColor,
                        child: const Text('Close'),
                      ),
                    ],
                  ));
        },
      ),
      body: Column(children: <Widget>[
        StreamBuilder(
            stream: chats.orderBy("datetime").snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              print(snapshot.data.documents);
              return Expanded(
                child: ListView(
                    controller: scontroller,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //reverse: true,
                    children: snapshot.data.documents.map((document) {
                      return ChatMessage(
                          message_content: document['message'],
                          username: document['sender']);
                    }).toList()),
              );
            }),
        TextField(
          controller: controller,
          onSubmitted: (value) async {
            print("Ls");
            var user = await FirebaseAuth.instance.currentUser();
            print(user);
            final String uid = user.uid;
            print(uid);
            String username = await getUsername(uid);
            chats.add({
              "sender": username,
              "message": value,
              "datetime": DateTime.now().toUtc().millisecondsSinceEpoch
            });
            controller.clear();
          },
        ),
      ]),
    );
  }
}
