import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hackthon/pages/chat.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MatchmakePage extends StatefulWidget {
  final String topic;

  const MatchmakePage({Key key, this.topic}) : super(key: key);

  @override
  MatchmakePageState createState() => MatchmakePageState();
}

Future<String> getid() async {
  var u = await FirebaseAuth.instance.currentUser();
  return u.uid;
}

class MatchmakePageState extends State<MatchmakePage> {
  bool islooking = false;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: ListView(
        children: [
          Center(
            child: FlatButton(
                child: Text("Get a partner"),
                onPressed: () {
                  Firestore.instance
                      .collection('topics')
                      .where('uid', isEqualTo: widget.topic)
                      .getDocuments()
                      .then((docs) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChatPage(
                          docid: docs.documents.first['chatid'],
                        ),
                      ),
                    );
                  });
                }

                /*child: StreamBuilder(
            stream: Firestore.instance
                .collection("matchmaking")
                .where("topic", isEqualTo: widget.topic)
                .where("matched", isEqualTo: false)
                .orderBy("waiting")
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  

              if (!islooking) {
                return FlatButton(
                  child: Text("Get a partner"),
                  onPressed: () {
                    setState(() {
                      islooking = true;
                    });
                  },
                );
              }

              Firestore.instance.collection('matchmaking').add({
                  "matched": false,
                  "topic": widget.topic,
                  "waiting": DateTime.now().toUtc().millisecondsSinceEpoch,
                  "uid": getid()
                });

              if (snapshot.data.documents.length == 0) {
                
                return CircularProgressIndicator();
              }

              DocumentSnapshot doc;

              if (snapshot.data.documents.length >= 2) {
                if (snapshot.data.documents.first['uid'] != getid()) {
                  doc = snapshot.data.documents.first;
                } else {
                  doc = snapshot.data.documents[1];
                }

                
            
                  Firestore.instance
                      .collection('chats')
                      .add({"topic": widget.topic}).then(
                          (val) => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ChatPage(docid: val.documentID)),
                              ));
                } 

                Firestore.instance.collection('matchmaking').where('uid', isEqualTo: getid());
              }
              

              return FlatButton(
                child: Text("join chat"),
                onPressed: () async {
                  Firestore.instance
                      .collection("matchmaking")
                      .where("uid", isEqualTo: await getid())
                      .getDocuments()
                      .then((docs) {
                    var id = docs.documents[0].documentID;
                    Firestore.instance
                        .collection('matchmaking')
                        .document(id)
                        .delete();
                  });
                },
              );
            },
          */
                ),
          ),
        ],
      ),
    );
  }
}
