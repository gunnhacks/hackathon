import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

class AddTopic extends StatefulWidget {
  AddTopic({Key key}) : super(key: key);
  @override
  AddTopicState createState() => AddTopicState();
}

class AddTopicState extends State<AddTopic> {
  final controller = TextEditingController();
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  CollectionReference
      topics = /*Firebase*/ Firestore.instance.collection('topics');
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.chat),
            title: Text("Add a topic"),
            subtitle: Container(
              height: 200,
              child: TextField(
                controller: controller,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                  child: Text("Crate"),
                  onPressed: () {
                    String name = controller.text;

                    Firestore.instance
                        .collection('chats')
                        .add({"topic": name}).then((document) {
                      topics.add({
                        "debates": [],
                        "name": name,
                        "tags": [],
                        "chat": document.documentID,
                        "uid": sha256.convert(utf8.encode(name)).toString()
                      });
                    });
                    Navigator.pop(context);
                  })
            ],
          )
        ],
      ),
    );
  }
}
