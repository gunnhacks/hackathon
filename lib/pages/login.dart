import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hackthon/pages/topics.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  
  Future<FirebaseUser> _signin() async {
    final FirebaseAuth _firebase = FirebaseAuth.instance;
    final GoogleSignIn _google = GoogleSignIn();
    await _google.signIn();
    /*
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    */
    var user = await _firebase.currentUser();
    print(user.uid);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [Colors.grey[900], Colors.grey[700]])),
      child: Center(
          child: Padding(
        padding: EdgeInsets.only(left: 16.0, right: 16.0),
        child: ListView(
          children: <Widget>[Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 50.0),
                child: Text(
                  "Wrangle",
                  style: TextStyle(
                    fontSize: 100.0,
                    color: Colors.grey,
                  ),
                ),
              ),
              Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Padding(
                        padding: EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                      bottom: 8.0,
                      top: 8.0,
                    )),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16.0,
                        bottom: 16.0,
                        left: 16.0,
                      ),
                      child: FlatButton(
                        color: Colors.grey,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0)),
                        onPressed: () async {
                          // Validate will return true if the form is valid, or false if
                          // the form is invalid.
                          //await _signin();
                          await FirebaseAuth.instance.signInAnonymously();
                          if (_formKey.currentState.validate()) {
                            // Anonymously login

                            // Process data.
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TopicsPage()));
                          }
                        },
                        child: Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text(
                              "Login",
                              style: TextStyle(
                                fontSize: 25.0,
                              ),
                            )),
                      ),
                    ),
                  ],
                  ),
                  ),
              ],
          )],
        ),
      )),
    ));
  }
}
