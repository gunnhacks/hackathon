import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

void setusername(String username) async {
  var user = await FirebaseAuth.instance.currentUser();
  Firestore.instance
      .collection('users')
      .add({"uid": user.uid, "username": username});
}

class ChangeusernamePage extends StatefulWidget {
  @override
  ChangeusernamePageState createState() => ChangeusernamePageState();
}

class ChangeusernamePageState extends State<ChangeusernamePage> {
  TextEditingController _controller = TextEditingController();
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Center(
      child: Column(
        children: [
          TextField(
            controller: _controller,
          ),
          FlatButton(
            child: Text("Change username"),
            onPressed: () async {
              setusername(_controller.text);
              Navigator.pop(context);
            },
          )
        ],
      ),
    ));
  }
}
