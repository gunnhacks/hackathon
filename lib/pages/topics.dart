import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:hackthon/pages/login.dart';
import 'package:hackthon/widgets/topic_view.dart';
import 'package:hackthon/pages/chat.dart';
import 'package:hackthon/pages/add_topic.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hackthon/pages/username.dart';

class TopicsPage extends StatefulWidget {
  TopicsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TopicsPageState createState() => new _TopicsPageState();
}

class _TopicsPageState extends State<TopicsPage> {
  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Wrangle'),
      ),

      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text('Wrangle', style: TextStyle(
                fontSize: 24,
              )),
              decoration: BoxDecoration(
                color: Colors.blue[300]
              ),
              ),
            
            ListTile(
              title: Text('Enter Test Chat'),
              onTap: () {
                setState(() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ChatPage(docid: "ykrt4Cf8EBou27d4BmPL")));
                      });
              }
            ),
            ListTile(
              title: Text('Change Username'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ChangeusernamePage()),
                  );
              }
            ),
            ListTile(
              title: Text('Sign Out'),
              onTap: () async{
                await FirebaseAuth.instance.signOut();
                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => LoginPage()));
              }
            ),
            
          ],
        ),
        ),
      

      floatingActionButton: FloatingActionButton(
        onPressed: () {
           Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                          Scaffold(
                              body: Padding(
                                padding: const EdgeInsets.fromLTRB(0, 16, 0, 16),
                                child: AddTopic(),
                              ))));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          /*
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              decoration: const InputDecoration(
                hintText: 'Search',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15.0),
                  ),
                ),
                filled: true,
                fillColor: Colors.grey,
              ),
            ),
          ), */
          Expanded(child: TopicView()),
        ],
      ),
    );
  }
}
